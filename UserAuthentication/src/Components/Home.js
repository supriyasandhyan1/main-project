import React, { Component } from 'react';
import { logout, isLogin } from './Util';
import { Link } from 'react-router-dom';

class Home extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLogin: isLogin()
        }
    }

    handleLogout = () => {
        logout();
        this.setState({
            isLogin: false
        })
    }

    render() {
        return (
            <div className="homm">
                <h1>Home</h1>
                <h3>first home page</h3>
            
                {this.state.isLogin ? 
                <button onClick={() => this.handleLogout()}>Click here to Logout</button> 
                 : <Link to="/signin">Go to sign in page</Link>
                }
            </div>
        );
    }
}

export default Home;