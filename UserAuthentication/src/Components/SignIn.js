import React from 'react';
import { login } from './Util';

const SignIn = (props) => {

    const handleLogin = () => {
        login();
        props.history.push('/dashboard');
    }


    return (
        <div className="Sign">
            <h1>Sign in</h1>
            <form>

                <label htmlFor='name'>Name</label>
                <input 
                type='text' 
                id='name' 
                name='name' />
    
                <label htmlFor='email'>E-mail</label>
                <input 
                type='email' 
                id='email' 
                name='email' />

                <label htmlFor='password'>password</label>
                <input 
                type='password' 
                id='password' 
                name='password' />

                <button onClick={() => handleLogin()}>Submit</button>

            </form>
        </div>
    );
};

export default SignIn;