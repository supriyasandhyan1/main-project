import React from 'react';


function Nav(){
    return(
        <nav className="navbar">
            <a href="/" className="navbar-logo">User</a>
            
            <ul className="navbar-links">

            <li className="navbar-dropdown">
                <a href="/home">Home</a>
            </li>

            <li className="navbar-dropdown">
                <a href="/Registration">Register</a>
            </li>

            <li className="navbar-dropdown">
                <a href="/Dashboard">Dashboard</a>
            </li>

            </ul>
        </nav>
    );
}
export default Nav;