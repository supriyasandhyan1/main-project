import React, { Component } from 'react';
import { BrowserRouter, Switch } from 'react-router-dom';
import Nav from './Components/Nav';
import User from './Components/User';
import Home from './Components/Home';
import Dashboard from './Components/Dashboard';
import SignIn from './Components/SignIn';
import Registration from './Registration';
import PrivateRoute from './Components/PrivateRoute';
import PublicRoute from './Components/PublicRoute';
import './App.css';

class App extends Component {

  render() {
    return (
      <BrowserRouter>
      <div> 
          <Nav />
         </div>
        <Switch>
          <PublicRoute restricted={false} component={User} path="/" exact />
          <PublicRoute restricted={false} component={Home} path="/Home" exact />
          <PublicRoute restricted={true} component={SignIn} path="/signin" exact />
          <PublicRoute component={ Registration } path="/Registration" exact />
          <PrivateRoute component={Dashboard} path="/dashboard" exact />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;