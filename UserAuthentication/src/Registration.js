import React, { Component} from 'react';

class Registration extends Component {
    constructor(props) {
        super(props)

        this.state = {
            fullName: "",
            mobileNumber: "",
            password: "",
        
        }
        this.handleSubmit=this.handleSubmit.bind(this)
    }

    firsthandler = (event) => {
        this.setState({
            fullName: event.target.value
        })
    }
    lasthandler = (event) => {
        this.setState({
            mobileNumber: event.target.value
        })
    }
    passwordhandler = (event) => {
        this.setState({
            password: event.target.value
        })
    }


    handleSubmit = (event) => {
        alert(`${this.state.fullName} ${this.state.mobileNumber}  Registered Successfully !!!!`)
        console.log(this.state);
        this.setState({
            fullName: "",
            mobileNumber: "",
            password: '',
            
        })
     event.preventDefault()
        
    }
    render() {
        return (
            <div className="Register">

                <form onSubmit={this.handleSubmit}>
                    <h1>Registration Form</h1>
                    <label>Full Name :</label> <input type="text" value={this.state.fullName} onChange={this.fullhandler} placeholder="Enter Full Number" /><br />
                    <label>Mobile Number :</label> <input type="text" value={this.state.mobileNumber} onChange={this.mobilehandler} placeholder="Enter Mobile Number" /><br />
                    <label>Password :</label> <input type="password" value={this.state.password} onChange={this.passwordhandler} placeholder="Enter Password" /><br />
                    <button>Submit</button>
                    {/* <input type="submit" value="Submit" className="bttn" /> */}
                </form>

            </div>
            
        );
    }
}
export default Registration;