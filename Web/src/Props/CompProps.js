import React from 'react';

const CompProps = (props) => {
    return(
        <div className="CompProps">
             <h1>Components & Props</h1>
             <button onClick={props.toggle}>show data</button>
            <h3> Introduction to React Js </h3>
            <p>React Introduction : {props.intro} </p>

        </div>
    );
}
export default CompProps;
