import React, { Component } from 'react';

class SwitchToggle extends Component {

  constructor(props){
    super(props);
    this.state = {
      isToggleOn: false
    }
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick = () => {
    this.setState(state => ({
      isToggleOn: !state.isToggleOn
    }));
  }

  render(){
    return(
      <div className="switchtoggle">
        <h1> Conditional Rendering </h1>
        <p> Switch will display ON when toggle is ON and show OFF otherwise. </p>
      <div className="toggler">
        <button onClick={this.handleClick}>
          { this.state.isToggleOn ? "ON" : "OFF"}
        </button>
      </div>
      </div>
    );
  }
}

export default SwitchToggle;