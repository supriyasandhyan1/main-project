import React, { useState } from 'react';
import { BrowserRouter as Router , Route , Switch } from 'react-router-dom';
import Navbar from './Components/Navbar';
import Home from './Components/Home';
import About from './Components/About';
import Tasks from './Components/Tasks';
import Hooks from './Components/Hooks';
import Contact from './Components/Contact';
import Form from './Form/Form';
import CompProps from './Props/CompProps';
import SwitchToggle from './ConditionalRender/SwitchToggle';
  
function App (){
  const [state, setState] = useState('')
  const [condition, setCondition] = useState(false)
  const  toggle =()=>{
    if(condition === false){
      setCondition(true)
      setState("React is a declarative, efficient, and flexible JavaScript library for building user interfaces. It lets you compose complex UIs from small and isolated pieces of code called “components”.")
    }
    else{
        setCondition(false)
        setState('')
    }
  }
  
  return(
      <Router>
        <div> 
          <Navbar />
         </div>
        <Switch>
          <Route exact path="/Home" component={Home}/>
          <Route exact path="/About" component={About}/>
          <Route exact path="/Tasks" component={Tasks}/>
          <Route exact path="/hooks" component={Hooks}/>
          <Route exact path="/form" component={Form}/>
          <Route exact path='/CompProps'>
          <CompProps intro={state} toggle={toggle} />
          </Route>
          <Route exact path="/Contact" component={Contact}/>
          <Route exact path="/switchToggle" component={SwitchToggle}/>
        </Switch>
      </Router> 
      
    );
}
export default App;