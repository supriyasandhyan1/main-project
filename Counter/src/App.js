import "./App.css";
import Counter from "./components/counter";

export default function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1 className="App-title">Simple Counter</h1>
      </header>
      <Counter />
    </div>
  );
}