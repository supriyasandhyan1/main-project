import React from "react";
// import "../App.css";
// import { useSelector, useDispatch } from "react-redux";
import { incNumber,decNumber,multiNum,divideNum,clearField} from "../actions/action";

const Counter = () => {
  const myState = useSelector((state) => state.changeTheNumber);
  const myDispatch = useDispatch();
  return (
    <div className="container">
      <h1> Counter </h1>
      <h1> {myState} </h1>
      <button onClick={() => myDispatch(divideNum())}>Divide</button>
      <button onClick={() => myDispatch(decNumber())}>Decrement</button>
      <button onClick={() => myDispatch(incNumber())}>Increment</button>
      <button onClick={() => myDispatch(multiNum())}>Multiply</button>
      <button onClick={() => myDispatch(clearField())}>Reset</button>
    </div>
  );
};
export default Counter;
