import changeTheNumber from "./index";

import { combineReducers } from "redux";

const rootReducer = combineReducers({
  changeTheNumber
});

export default rootReducer;
