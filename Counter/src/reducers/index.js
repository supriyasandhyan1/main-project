const initialState = 0;
const changeTheNumber = (state = initialState, action) => {
  switch (action.type) {
    case "INCREMENT":
      return state + 1;

    case "DECREMENT":
      return state - 1;

    case "MULTI":
      return state * 2;

    case "DIVIDE":
      return state / 2;

    case "CLEAR":
      return state - state;

    default:
      return state;
  }
};

export default changeTheNumber;
