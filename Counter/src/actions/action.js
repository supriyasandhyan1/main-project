export const incNumber = () => {
    return { type: "INCREMENT" };
  };
  
  export const decNumber = () => {
    return { type: "DECREMENT" };
  };
  
  export const multiNum = () => {
    return { type: "MULTI" };
  };
  
  export const divideNum = () => {
    return { type: "DIVIDE" };
  };
  
  export const clearField = () => {
    return { type: "CLEAR" };
  };
  